﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;

namespace PlantGenetics
{
    class Seed 
    {
        int dna;
    }
    class Plant 
    {
        int water_retention;
        int height;
        int energy_in_seeds;
        Cold tolerance

        int dna;
    }
    public class Plot 
    {
        int waterPortions;
        Plant[] plants = new Plant[10*10];
    }

    public class Sim
    {
        int[] provinces;

        public Sim()
        {
            if (provinces == null)
            {
                ReadWorldFile("../../../Input/Hased/Hased_0_0_2500.txt");
            }
        }

        void ReadWorldFile(string myFile)
        {
            string[] lines = System.IO.File.ReadAllLines(myFile);

            string name = lines[0];
            string date = lines[1];
            string dimensions = lines[2];

            Console.WriteLine(name);

            string[] dateTokens = date.Split('_');
            int month = int.Parse(dateTokens[0]) + 1;
            int day = int.Parse(dateTokens[1]) + 1;
            int year = int.Parse(dateTokens[2]);

            Console.WriteLine(CultureInfo.CurrentCulture.DateTimeFormat.GetMonthName(month) + " " + day + " " + year);

            string[] dimensionsTokens = dimensions.Split('x');
            int columns = int.Parse(dimensionsTokens[0]);
            int rows = int.Parse(dimensionsTokens[1]);

            Console.WriteLine(columns + " by " + rows);

            provinces = new int[rows*columns];

            //foreach (string line in lines)
            //{
            //    // Use a tab to indent each line of the file.
            //    Console.WriteLine("\t" + line);
            //}
        }
    }
}
