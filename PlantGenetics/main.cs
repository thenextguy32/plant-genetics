﻿using PlantGenetics;
using System;
namespace HelloWorld
{
    class Maine
    {
        static void Main()
        {
            Sim simulation = new Sim();

            // Keep the console window open in debug mode.
            Console.WriteLine("Press any key to exit.");
            Console.ReadKey();
        }
    }
}